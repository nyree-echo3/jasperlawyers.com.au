<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Module;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin/dashboard');
    }

    public function dashboard()
    {
        return view('admin/dashboard');
    }

    public function pagination(Request $request)
    {
        session()->put('pagination-count',$request->pagination_count);
        return redirect()->back();
    }

    public function sidebarState()
    {
        $state = session('sidebar-state');

        if($state==''){
            session(['sidebar-state' => 'sidebar-collapse']);
        }else{
            session(['sidebar-state' => '']);
        }
    }

}
