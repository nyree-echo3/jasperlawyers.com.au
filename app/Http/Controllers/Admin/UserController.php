<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->role=='admin'){
            $users = User::all();
        }else{
            $users = User::where('role','!=','admin')->get();
        }

        return view('admin/user/user', compact('users'));
    }

    public function add()
    {
        if(Auth::user()->role=='admin'){
            $users = User::all();
        }else{
            $users = User::where('role','!=','admin')->get();
        }
        return view('admin/user/add',compact('users'));
    }

    public function edit($user_id)
    {
        $user = User::find($user_id);
        $user_permissions = Permission::where('module_id','=',0)->orderBy('order_id')->get();
        $settings_permissions = Permission::where('module_id','=',-1)->orderBy('order_id')->get();

        $module_permissions = Module::with('permissions')->orderBy('position', 'desc')->get();

        foreach ($user_permissions as $permission){
            $permission->user_has = false;
            if($user->can($permission->name)){
                $permission->user_has = true;
            }
        }

        foreach ($settings_permissions as $permission){
            $permission->user_has = false;
            if($user->can($permission->name)){
                $permission->user_has = true;
            }
        }

        foreach ($module_permissions as $module){
            foreach ($module->permissions as $permission){
                $permission->user_has = false;
                if($user->can($permission->name)){
                    $permission->user_has = true;
                }
            }
        }

        return view('admin/user/edit', array(
            'user' => $user,
            'user_permissions' => $user_permissions,
            'settings_permissions' => $settings_permissions,
            'module_permissions' => $module_permissions,
        ));
    }

    public function delete($user_id)
    {
        $user = User::where('id','=',$user_id)->first();
        $user->is_deleted = true;
        $user->save();

        return \Redirect::back()->with('message', Array('text' => 'User has been deleted.', 'status' => 'success'));
    }

    public function changeUserStatus(Request $request, $user_id)
    {
        $user = User::where('id', '=', $user_id)->first();
        if ($request->status == "true") {
            $user->status = 'active';
        } else if ($request->status == "false") {
            $user->status = 'passive';
        }
        $user->save();

        return Response::json(['status' => 'success']);
    }

    public function savePermission(Request $request)
    {
        $user = User::find($request->user_id);
        $selected_permissions = array();
        if($request->permissions){
            $selected_permissions = $request->permissions;
        }

        $permissions = Permission::all();
        foreach ($permissions as $permission){
            if(in_array($permission->name,$selected_permissions)) {
                if(!$user->can($permission->name)){
                    $user->givePermissionTo($permission->name);
                }
            }else{
                if($user->can($permission->name)){
                    $user->revokePermissionTo($permission->name);
                }
            }
        }

        return \Redirect::to('dreamcms/user/'.$user->id.'/edit')->with('message', Array('text' => 'Permissions has been updated.', 'status' => 'success'));
    }

    public function savePassword(Request $request)
    {
        $rules = array(
            'password' => 'required|min:6|same:password',
            'password_confirmation' => 'required|same:password'
        );
        $messages = [
            'password.required' => 'Please enter new password',
            'password.min' => 'Password can not be less than 6 characters',
            'password_confirmation.required' => 'Please enter new password',
            'password_confirmation.same' => 'Passwords do not match',
        ];

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator);
        }

        $user = User::find($request->user_id);
        $user->password = Hash::make($request->password);
        $user->save();

        return \Redirect::to('dreamcms/user/'.$user->id.'/edit')->with('message', Array('text' => 'Password has been changed.', 'status' => 'success'));

    }

    public function saveInformation(Request $request)
    {

        $rules = array(
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique_update:users,'.$request->user_id
        );
        $messages = [
            'name.required' => 'Please enter a name',
            'email.required' => 'Please enter an email address',
            'email.email' => 'Please enter a valid email address',
            'email.unique_update' => 'The email address is in use'
        ];

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $user = User::find($request->user_id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return \Redirect::to('dreamcms/user/'.$user->id.'/edit')->with('message', Array('text' => 'User information has been changed.', 'status' => 'success'));

    }
}
