<?php

namespace App\Http\Middleware;

use Closure;

class Redirects
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $redirects = [
			'pages/about/about-jasper' => 'team/people/sy-jasper-kwok',
			
			'pages/services/contracts' => 'pages/services/commercial-law',          
			'pages/services/franchising' => 'pages/services/commercial-law', 						 
			'pages/services/business-structure' => 'pages/services/commercial-law', 
			'pages/services/corporate-advisory' => 'pages/services/commercial-law', 
			'pages/services/intellectual-property' => 'pages/services/commercial-law', 
			'pages/services/employment-law' => 'pages/services/commercial-law', 
			'pages/services/business-transactions' => 'pages/services/commercial-law', 
			'pages/services/commercial-leasing' => 'pages/services/commercial-law', 
			
			'pages/services/property-law' => 'pages/services/private-client-services', 			
			'pages/services/family-law' => 'pages/services/private-client-services', 						
			'pages/services/wills-probates' => 'pages/services/private-client-services', 
			
			'pages/services/government-actions' => 'pages/services/litigation', 
			'pages/services/commercial-disputes' => 'pages/services/litigation', 			
			'pages/services/civil-disputes' => 'pages/services/litigation', 
			
			'pages/other/privacy-policy' => 'pages/terms-of-use/privacy-policy', 
			'pages/other/terms-of-use' => 'pages/terms-of-use/privacy-policy', 
			
        ];



        foreach($redirects as $from => $to){

            if($request->path()==$from){

                return redirect($to,301);
            }
        }

        return $next($request);
    }
}
