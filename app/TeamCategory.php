<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class TeamCategory extends Model
{
    use SortableTrait;

    protected $table = 'team_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function members()
    {
        return $this->hasMany(TeamMember::class, 'category_id')->where('status', '=', 'active');
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','team')->where('type','=','category')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'team/'.$this->attributes['slug'];
    }
}
