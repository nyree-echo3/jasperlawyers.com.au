$('#added_image').sortable({
    items: '.sortable',

    stop: function () {
        counter = 0;
        $('input.imgPosition').each(function () {
            $(this).val(counter);
            counter++;
        });
    }
});

for (i = 0; i < $('#imageCount').val(); i++) {
    $('#imgStatus' + i).bootstrapToggle();
}

function openPopup() {
    imageCounter = $('#imageCount').val();

    CKFinder.popup({
        resourceType:'Images',
        chooseFiles: true,
        width: 800,
        height: 600,

        onInit: function (finder) {

            finder.on('files:choose', function (evt) {
                var files = evt.data.files;

                files.forEach(function (file, i) {
                    folder = file.get('folder');

                    strHtml = '<div id="imgBox' + imageCounter + '" class="image-box sortable">';

                    strHtml += '<input id="imgPosition' + imageCounter + '" name="imgPosition' + imageCounter + '" type="hidden" class="imgPosition" value="' + imageCounter + '">';
                    strHtml += '<input id="imgDelete' + imageCounter + '" name="imgDelete' + imageCounter + '" type="hidden" value="">';
                    strHtml += '<input id="imgType' + imageCounter + '" name="imgType' + imageCounter + '" type="hidden" value="new">';
                    strHtml += '<div class="image-box-toolbox">';

                    strHtml += '<a class="tool" title="Delete" href="#" onclick="image_delete(' + imageCounter + '); return false;">';
                    strHtml += '<i class="fa fa-trash-alt"></i>';
                    strHtml += '</a>';

                    strHtml += '<div class="pull-right" style="margin-right: 10px">';
                    strHtml += '<div style="width: 33px; height: 22px;">';
                    strHtml += '<input id="imgStatus' + imageCounter + '" name="imgStatus' + imageCounter + '" data-id="' + imageCounter + '" type="checkbox" class="image_status" data-size="mini" checked>';
                    strHtml += '</div>';
                    strHtml += '</div>';

                    strHtml += '</div>';

                    strHtml += '<div class="image-box-img">';
                    strHtml += '<image class="image-item" src="' + folder.getUrl() + file.get('name') + '">';
                    strHtml += '<input id="imgLocation' + imageCounter + '" name="imgLocation' + imageCounter + '" type="hidden" value="' + folder.getUrl() + file.get('name') + '">';

                    strHtml += '</div>';

                    strHtml += '</div>';

                    $('#added_image').append(strHtml);
                    $('#imgStatus' + imageCounter).bootstrapToggle();

                    imageCounter++;

                    $('#imageCount').val(imageCounter);
                    $('#remove-image').removeClass('invisible');
                });
            });

        }

    });
}

function image_delete(id) {
    $('#imgBox' + id).remove();
    $('#imageCount').val($('#imageCount').val() - 1);

    if ($('#imageCount').val() == 0) {
        $('#remove-image').addClass('invisible');
    }
}

function image_edit_delete(id)  {
    $('#imgBox' + id).addClass('invisible');
    $('#imgDelete' + id).val("1");
}