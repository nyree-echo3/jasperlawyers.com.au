<footer class='footer'>     
  <div class='footerContainer'>       
	  <div id="footer-txt"> 
	     <div class="container-fluid">
		     <div class="row no-gutters h-100 align-items-center">         			 			 
				 <div class="col-lg-1 col-sm-2"></div>

				 <div class="col-lg-6 col-sm-8 custom-padding">

					<a href="{{ url('') }}">&copy; {{ date('Y') }} {{ $company_name }}</a><span> |</span>
					<a href="{{ url('') }}/contact">Contact</a><span> |</span>
					<a href="{{ url('') }}/pages/terms-of-use/privacy-policy">Privacy Policy</a><span> |</span>
					<a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a>  
				 </div>
	        </div>
	    </div>
     </div>	  
  </div>
</footer>