<div class="index-panel-seo">
   <div class="container-fluid">
	  <div class="row no-gutters h-100 align-items-center">         			 			 
		 <div class="col-lg-1 col-sm-2"></div>

		 <div class="col-xl-6 col-lg-8 col-sm-8 custom-padding">
			  <div class="index-panel-seo-txt">			 
				  {!! $home_intro_text !!}					  
			  </div>
		  </div><!-- /.col-lg-6 -->	

		  <div class="col-lg-5 col-sm-2"></div>		

		</div><!-- /.row -->	
   </div><!-- /.container -->	
</div><!-- /.index-panel-seo -->	