<div class="background">
	<div class="container-fluid background-grid h-100">
	    <div class="row no-gutters h-100 align-items-center">
			@for ($i=1; $i<=12; $i++)
			  <div class="col-lg-1 col-sm-1 col-xs-1 h-100">			    
				  <div class="background-grid-border"></div>				 
			  </div>
			@endfor
		</div>
	</div>
</div>