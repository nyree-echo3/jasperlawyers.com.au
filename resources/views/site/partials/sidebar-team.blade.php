<div class="blog-sidebar">			
	<ol class="list-group list-unstyled list-group-flush">
	    @foreach ($team_members as $member)
			<li class='{{ ($member->url == $team_member->url ? "active" : "")}} list-group-item'>
			   <a class='navsidebar' href='{{ url('') }}/{{ $member->url }}'>
			      @if ($member->url == $team_member->url)
			          <h1>
			      @endif    
			          
				  {{ $member->name }}
				  
				  @if ($member->url == $team_member->url)
			          </h1>
			      @endif 
			   </a>
			</li>
		@endforeach
	</ol>	
</div>