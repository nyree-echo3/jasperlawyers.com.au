<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top btco-hover-menu navbar-custom col-lg-3">   
    <div class="container-fluid h-100">
		  <div class="row no-gutters h-100 align-items-center">		  			
				<button class="navbar-toggler custom-toggler hamburger hamburger--collapse hamburger--accessible js-hamburger" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="hamburger-box">
					<span class="hamburger-inner"></span>
				  </span>
				</button>


				<div class="collapse navbar-collapse h-100" id="navbarCollapse">
					<ul class="navbar-nav mr-auto h-100">							
						{!! $navigation !!}
					</ul>                     
				</div>			  
		  </div>
	</div>
</nav>

@section('inline-scripts-navigation')
    <script type="text/javascript">
		  var $hamburger = $(".hamburger");
		  $hamburger.on("click", function(e) {
			$hamburger.toggleClass("is-active");
			// Do something else, like open/close menu
		  });
    </script>
@endsection