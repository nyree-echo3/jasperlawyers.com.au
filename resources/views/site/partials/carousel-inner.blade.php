@php
if (!isset($header_image) || $header_image == "")  {
   $header_image = url('') . "/images/site/slider-image.png";
}
@endphp

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="slide" src="{{ $header_image }}" alt="Header Image">
            <div class="container">
                <div class="carousel-caption text-left">
                </div>
            </div>
        </div>
    </div>
    <div class="carousel-inner-overlay"></div>
</div>