<!-- Navbar -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark btco-hover-menu">	
     <div class="navbar-logo">
     <a href="{{ url('') }}/index" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
  </div>
  
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item {{ (!isset($page_type) ? "active" : "") }}">
				<a class="nav-link" href="{{ url('') }}"><i class='fa fa-home'></i> <span class="sr-only">(current)</span></a>
			</li>
			@if(count($navigation))
               @foreach($navigation as $nav_item)    
					<li class="nav-item dropdown">
						<a class="nav-link {{ (isset($page_type) && (($page_type == "Pages" && $category[0]->slug == $nav_item["slug"]) || ($page_type != "Pages" && $page_type == $nav_item["name"])) ? "active" : "") }}" href="{{ url('').'/'.$nav_item["url"] }}" id="navbarDropdownMenuLink">
							{{ $nav_item["display_name"] }}
						</a>
						
						@if(isset($nav_item["nav_sub"]) && sizeof($nav_item["nav_sub"]) > 0)
							<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							    
							    @foreach($nav_item["nav_sub"] as $nav_sub)	
								
									<li><a class="dropdown-item" href="{{ url('').'/'.$nav_sub["url"] }}">{{ (isset($nav_sub["title"]) ? $nav_sub["title"] : $nav_sub["name"]) }}</a>
									
									    @if(isset($nav_item["nav_sub_sub"]))
											<ul class="dropdown-menu">
												@foreach($nav_item["nav_sub_sub"] as $nav_sub_sub)	
												   @if($nav_sub_sub["parent_page_id"] == $nav_sub["id"])														  
													  <li><a class="dropdown-item" href="{{ url('').'/'.$nav_sub_sub }}">{{ (isset($nav_sub_sub["title"]) ? $nav_sub_sub["title"] : $nav_sub_sub["name"]) }}</a></li>
												   @endif
												@endforeach											
											</ul>
										@endif
										
									</li>
								@endforeach
								
							</ul>
						@endif
						
					</li>
			   @endforeach
            @endif
                                    			
		</ul>
	  
		  <form class="form-inline mt-2 mt-md-0">
			  <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
			  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>

			<div class='navbar-contacts'>
			  <a href='tel:{{ str_replace(' ', '', $phone_number) }}'><i class='fa fa-phone'></i> {{ $phone_number }}</a>
			  <a href='{{ url('') }}/contact'><i class='fa fa-envelope'></i> Contact Us</a>
			</div>
			
			@include('site/partials/helper-shop')
	</div>
</nav>