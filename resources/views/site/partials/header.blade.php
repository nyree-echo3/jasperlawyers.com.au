<div class="navbar-panel">
	<div class="container-fluid">
		<div class="row no-gutters h-100 align-items-center">

		  <div class="col-lg-1 col-sm-4 col-xs-4"></div>

		  <div class="col-lg-2 col-sm-4 col-xs-4">  
			  <div class="navbar-logo">
				  <a href="{{ url('') }}" title="{{ $company_name }}">
				     <img class="logo-desktop" src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}">
				     <img class="logo-mobile" src="{{ url('') }}/images/site/logo-mobile.gif" title="{{ $company_name }}" alt="{{ $company_name }}">
				  </a>
			  </div>
		  </div>			  
		</div>
	</div>
</div>