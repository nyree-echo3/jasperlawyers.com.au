@if (isset($home_services))
	<div class="index-services"> 
	   <div class="container-fluid">
		  <div class="row">   
			  <div class="col-lg-1 d-md-none d-lg-block"></div>
			  
			  @foreach ($home_services as $home_service) 
			     <div class="col-sm-2 d-lg-none d-xl-none"></div>
			      		   	   		   	 
				 <div class="col-lg-2 col-sm-8 custom-padding">	 
				    <div class="index-service">         				        
						<h2><a href="{{ url('')}}/{{ $home_service->url }}">{{ $home_service->title }}</a></h2>
						{!! $home_service->short_description !!}																		
						<div class="d-none d-sm-none d-md-none d-lg-block"><a href="{{ url('')}}/{{ $home_service->url }}">learn more</a></div>
						<div class="index-service-more d-lg-none d-xl-none"><a href="{{ url('')}}/{{ $home_service->url }}"><i class="fas fa-chevron-right"></i></a></div>
					</div>
				 </div><!-- /.col-lg-2 --> 
				 
				 <div class="col-sm-2 d-lg-none d-xl-none"></div> 		   	  		   	 
			  @endforeach  		   	 


	</div>
@endif
