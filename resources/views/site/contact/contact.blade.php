<?php 
   // Set Meta Tags
   $meta_title_inner = 'Jasper Lawyers | Contact'; 
   $meta_keywords_inner = 'Jasper Lawyers | Contact'; 
   $meta_description_inner = 'Jasper Lawyers | Contact'; 
?>

@extends('site/layouts/app')

@section('content')

<div class="contact-panel">
   <div class="container-fluid">
	  <div class="row no-gutters h-100">         			 			 
		 <div class="col-lg-1 col-sm-2"></div>

		 <div class="col-xl-3 col-lg-3 col-sm-8 custom-padding">
			  <div class="contact-panel-wrapper">	
			      <div class="contact-panel-wrapper-txt">
				     <h1 class="blog-masthead-h1">Contact Details</h1> 
				     <div>{!! $contact_details !!}</div>
				  </div>		 		  
			  </div>
		  </div><!-- /.col-lg-3 -->	

		  <div class="col-xl-8 col-lg-8 col-sm-10 custom-padding">
			  <div class="contact-panel-img">
			     <img src="{{ url('') }}/images/site/normanby-chambers.png" title="Normanby Chambers" alt="Normanby Chambers">	 
			  </div>
		  </div>		

		</div><!-- /.row -->	
   </div><!-- /.container -->	
</div><!-- /.index-panel-seo -->

@endsection
