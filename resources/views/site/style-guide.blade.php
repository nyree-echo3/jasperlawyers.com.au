@extends('site/layouts/app')

@section('content')

<div class="blog-masthead">            
   <div class="container-fluid">
	  <div class="row no-gutters h-100 align-items-center">         			 			 
		 <div class="col-lg-1"></div>

		 <div class="col-lg-6 custom-padding">          
            <h1>Heading 1 lorem ipsum</h1>
            
            <div data-aos="fade-up" data-aos-duration="2000">	
				<p>P Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>

				<blockquote>
				<p>Blockquote Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
				</blockquote>

				<ul>
					<li>List Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
					<li>List Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
				</ul>

				<hr />
				<h2>Heading 2 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h2>

				<h3>Heading 3 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h3>

				<h4>Heading 4 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h4>

				<h5>Heading 5 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h5>
            </div>
           
		 </div><!-- /.col-lg-6 -->		

		</div><!-- /.row -->	
   </div><!-- /.container -->	
</div><!-- /.blog-masthead -->	

@endsection
