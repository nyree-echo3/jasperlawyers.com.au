<?php 
   // Set Meta Tags
   $meta_title_inner = $page->meta_title; 
   $meta_keywords_inner = $page->meta_keywords; 
   $meta_description_inner = $page->meta_description;  
?>

@extends('site/layouts/app')

@section('content')

<div class="blog-masthead">            
   <div class="container-fluid">
	  <div class="row no-gutters h-100 align-items-center">         			 			 
		 <div class="col-lg-1"></div>

		 <div class="col-lg-7 custom-padding">			     	    		    			
			 <div class="blog-masthead-h1">{{ $category[0]->name }}</div>
			 @include('site/partials/sidebar-pages')
			 
			 <div>
			    {!! $page->body !!}		
			 </div>
		 </div><!-- /.col-lg-7 -->		

		</div><!-- /.row -->	
   </div><!-- /.container -->	
</div><!-- /.blog-masthead -->	
       
@endsection
