<?php 
   // Set Meta Tags
   $meta_title_inner = 'Jasper Lawyers | ' . $category[0]->name; 
   $meta_keywords_inner = 'Jasper Lawyers | ' . $category[0]->name; 
   $meta_description_inner = 'Jasper Lawyers | ' . $category[0]->name; 
?>

@extends('site/layouts/app')

@section('content')

<div class="blog-masthead">            
   <div class="container-fluid">
	  <div class="row no-gutters h-100 align-items-center">         			 			 
		 <div class="col-lg-1"></div>

		 <div class="col-lg-7 custom-padding">		    		    			
			 <div class="blog-masthead-h1">{{ $category[0]->name }}</div>
			 
			 @php
                $counter = 1;
              @endphp
                                                   
			  @foreach($side_nav as $item) 
				  @php
					 $counter++;
				  @endphp

				  @if ($counter % 2 == 0)
					  @if ($counter != 2)
						 </div>
					  @endif
					  <div class="grid">																						                                                    
				  @endif

				  <div class='grid-column'>
					  <div class="page-list">         
						  <h2><a href="{{ url('')}}/{{ $item->url }}">{{ $item->title }}</a></h2>
						  {!! $item->short_description !!}
						
						  <div class="d-none d-sm-none d-md-none d-lg-block"><a href="{{ url('')}}/{{ $item->url }}">learn more</a></div>
						  <div class="pages-category-more d-lg-none d-xl-none"><a href="{{ url('')}}/{{ $item->url }}"><i class="fas fa-chevron-right"></i></a></div>
					  </div>			
				  </div>

				  @if ($counter % 2 == 0)
					  <div class='grid-spacer'><p>&nbsp;</p></div>																							                                                    
				  @endif					  					  
			   @endforeach                                                       		                                      		 			 			 			 
					
		 </div><!-- /.col-lg-7 -->		

		</div><!-- /.row -->	
   </div><!-- /.container -->	
</div><!-- /.blog-masthead -->	
       
@endsection
