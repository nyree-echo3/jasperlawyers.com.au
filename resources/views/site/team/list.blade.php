<?php 
   // Set Meta Tags
   $meta_title_inner = 'Jasper Lawyers | People'; 
   $meta_keywords_inner = 'Jasper Lawyers | People'; 
   $meta_description_inner = 'Jasper Lawyers | People'; 
?>

@extends('site/layouts/app')

@section('content')

<div class="blog-masthead">            
   <div class="container-fluid">
	  <div class="row no-gutters h-100 align-items-center">         			 			 
		 <div class="col-lg-1"></div>

		 <div class="col-lg-7 custom-padding">  
                   
            <h1>People</h1>
	              
	        @php
               $counter = 1;
            @endphp
                  
            @if (isset($items))                          
				  @foreach($items as $item) 
				      @php
                         $counter++;
                      @endphp
                      
                      @if ($counter % 2 == 0)
                          @if ($counter != 2)
                             </div>
                          @endif
					      <div class="grid">																						                                                    
					  @endif
					              								            								
					  <div class='grid-column'>
						<div class='page-list team-list'>
							<h2><a href="{{ url('')}}/{{ $item->url }}"><span>{{ $item->name }}</span> | {{ $item->job_title }}</a></h2>
							
							<p>
							@if($item->phone != "") T <a href='tel:{{ str_replace(" ", "", $item->phone) }}'>+{{ $item->phone }}</a><br> @endif
			                @if($item->mobile != "") M <a href='tel:{{ str_replace(" ", "", $item->mobile) }}'>+{{ $item->mobile }}</a><br> @endif
			                @if($item->email != "") E <a href='mailto:{{ $item->email }}'>{{ $item->email }}</a> @endif	
							</p>
							
							<div class="d-none d-sm-none d-md-none d-lg-block"><a href='{{ url('') }}/{{ $item->url }}'>learn more</a></div>
							<div class="pages-category-more d-lg-none d-xl-none"><a href="{{ url('')}}/{{ $item->url }}"><i class="fas fa-chevron-right"></i></a></div>
						</div>						
					  </div>
					   
					  @if ($counter % 2 == 0)
					      <div class='grid-spacer'><p>&nbsp;</p></div>																							                                                    
					  @endif					  					  
				   @endforeach                                                       		                        
               @else
                 <p>Currently there is no people to display.</p>
               @endif

		 </div><!-- /.col-lg-7 -->		

		</div><!-- /.row -->	
   </div><!-- /.container -->	
</div><!-- /.blog-masthead -->	
       
@endsection
