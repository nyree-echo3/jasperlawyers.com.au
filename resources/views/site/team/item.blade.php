<?php 
   // Set Meta Tags
   $meta_title_inner = 'Jasper Lawyers | People | ' . $team_member->name ; 
   $meta_keywords_inner = 'Jasper Lawyers | People | ' . $team_member->name ; 
   $meta_description_inner = 'Jasper Lawyers | People | ' . $team_member->name ; 
?>

@extends('site/layouts/app')

@section('content')

<div class="blog-masthead">            
   <div class="container-fluid">
	  <div class="row no-gutters h-100 align-items-center">         			 			 
		 <div class="col-lg-1"></div>

		 <div class="col-lg-8 custom-padding">  
                   
            <h1>People</h1>
            @include('site/partials/sidebar-team')
            
            <div>  
				<div class="grid-people"> 
				   <div class="grid-column-wide">
					  <div class="team-name"><span>{{ $team_member->name }}</span> | {{ $team_member->job_title }}</div>
					  <div class="team-desc">{!! $team_member->body !!}</div>	
				   </div>

				   <div class="grid-spacer"><p>&nbsp;</p></div>

				   <div class="grid-column-narrow">
						<div class="team-desc">
						   <strong>Qualification</strong><br>
						   {!! $team_member->short_description !!}
						</div>

						<div class="team-contact">
						   <strong>Contact Details</strong><br>
						   @if($team_member->phone != "") <span>T</span><a href='tel:{{ str_replace(" ", "", $team_member->phone) }}'>+{{ $team_member->phone }}</a><br> @endif
						   @if($team_member->mobile != "")<span>M</span><a href='tel:{{ str_replace(" ", "", $team_member->mobile) }}'>+{{ $team_member->mobile }}</a><br> @endif
						   @if($team_member->email != "") <span>E</span><a href='mailto:{{ $team_member->email }}'>{{ $team_member->email }}</a> @endif
						</div>			        					
				   </div>				   
				</div> 
			</div>                                                       
			                                                           
		 </div><!-- /.col-lg-7 -->		

		</div><!-- /.row -->	
   </div><!-- /.container -->	
</div><!-- /.blog-masthead -->	
       
@endsection