<?php 
   // Set Meta Tags
   $meta_title_inner = "Page Not Found"; 
   $meta_keywords_inner = "Page Not Found"; 
   $meta_description_inner = "Page Not Found";  
?>

@extends('site/layouts/app')

@section('content')

<div class="blog-masthead">            
   <div class="container-fluid">
	  <div class="row no-gutters h-100 align-items-center">         			 			 
		 <div class="col-lg-1"></div>

		 <div class="col-lg-7 custom-padding">
            <h1>Oops :(</h1>
            <div data-aos="fade-up" data-aos-duration="2000">				
				<h2>Sorry, the page you are looking for is missing!</h2>

				<p>The page has expired or may have been unpublished. Please try the following:</p>

				<ul>
					<li>Go to the <a href="{{ url('') }}">Home page</a>.</li>
				<li>Follow the links on the menu above.</li>
				<li>Check the URL and try again.</li>
				</ul>
			</div>
       
		 </div><!-- /.col-lg-7 -->		

		</div><!-- /.row -->	
   </div><!-- /.container -->	
</div><!-- /.blog-masthead -->	
       
@endsection