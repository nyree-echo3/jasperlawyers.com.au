@extends('admin/layouts/app')

@section('styles')
<link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }} Sort</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/pages') }}"><i class="fas fa-file-alt"></i> {{ $display_name }}</a></li>
                <li class="active">Sort</li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="box-header">
                        <form id="category-form" method="post" class="form-horizontal" action="{{ url('dreamcms/pages/sort') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">

                                <div class="form-group col-sm-4">
                                    <label class="control-label col-sm-4">Category :</label>

                                    <div class="col-sm-8">

                                        <select id="category" name="category" class="select2" style="width: 100%;">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"{{ $selected_category_id == $category->id ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box-body">

                    @if(count($pages)>0)
                    <table class="table table-striped table-hover">
                        <tbody class="sortable" data-entityname="pages">
                        @foreach ($pages as $page)
                        <tr data-itemId="{{ $page->id }}">
                            <td class="sortable-handle"><span class="glyphicon glyphicon-sort"></span></td>
                            <td>
                                {{ $page->title }}

                                @if(count($page->childPages)>1)
                                    <a href="{{ url('dreamcms/pages/'.$page->id.'/child-sort') }}" type="button" class="btn btn-info btn-xs" data-widget="add">Sort Child Pages</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        No records
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        var changePosition = function(requestData){

        $.ajax({
            'url': '{{ url('dreamcms/sort') }}',
            'type': 'POST',
            'data': requestData,
            'success': function(data) {
                if (data.success) {

                    setNavigationPosition();

                } else {
                    toastr.options = {"closeButton": true}
                    toastr.error(data.errors);
                }
            },
            'error': function(){
                toastr.options = {"closeButton": true}
                toastr.error('Something wrong');
            }
        });
    };

    function setNavigationPosition(){

        $.ajax({
            'url': '{{ url('dreamcms/pages/navigation-page-sort') }}',
            'type': 'POST',
            'success': function(response) {
                if(response.status=="success"){

                    toastr.options = {"closeButton": true}
                    toastr.success('Saved');
                } else {
                    toastr.options = {"closeButton": true}
                    toastr.error('Something wrong');
                }
            },
            'error': function(){
                toastr.options = {"closeButton": true}
                toastr.error('Something wrong');
            }
        });
    }

    $(document).ready(function(){

        $(".select2").select2();

        $('#category').on('select2:select', function (e) {
            $('#category-form').submit();
        });

        var $sortableTable = $('.sortable');
        if ($sortableTable.length > 0) {
            $sortableTable.sortable({
                handle: '.sortable-handle',
                axis: 'y',
                update: function(a, b){

                    var entityName = $(this).data('entityname');
                    var $sorted = b.item;

                    var $previous = $sorted.prev();
                    var $next = $sorted.next();

                    if ($previous.length > 0) {
                        changePosition({
                            parentId: $sorted.data('parentid'),
                            type: 'moveAfter',
                            entityName: entityName,
                            id: $sorted.data('itemid'),
                            positionEntityId: $previous.data('itemid')
                        });
                    } else if ($next.length > 0) {
                        changePosition({
                            parentId: $sorted.data('parentid'),
                            type: 'moveBefore',
                            entityName: entityName,
                            id: $sorted.data('itemid'),
                            positionEntityId: $next.data('itemid')
                        });
                    } else {
                        console.error('Something wrong!');
                    }
                },
                cursor: "move"
            });
        }
    });
    </script>
@endsection