@extends('admin/layouts/app')

@section('styles')
<link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }} Sort</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/pages') }}"><i class="fas fa-file-alt"></i> {{ $display_name }}</a></li>
                <li class="active">Sort</li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $parent_page->title  }} - Child Pages</h3>

                    <div class="pull-right box-tools">
                        <form id="category-form" method="post" class="form-horizontal" action="{{ url('dreamcms/pages/sort') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="category" value="{{ $parent_page->category_id }}">
                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fas fa-arrow-left"></i> Back
                            </button>
                        </form>
                    </div>
                </div>
                <div class="box-body">

                    @if(count($pages)>0)
                    <table class="table table-striped table-hover">
                        <tbody class="sortable" data-entityname="pages">
                        @foreach ($pages as $page)
                        <tr data-itemId="{{ $page->id }}">
                            <td class="sortable-handle"><span class="glyphicon glyphicon-sort"></span></td>
                            <td>{{ $page->title }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        No records
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        var changePosition = function(requestData){

        $.ajax({
            'url': '{{ url('dreamcms/sort') }}',
            'type': 'POST',
            'data': requestData,
            'success': function(data) {
                if (data.success) {

                    setNavigationPosition();

                } else {
                    toastr.options = {"closeButton": true}
                    toastr.error(data.errors);
                }
            },
            'error': function(){
                toastr.options = {"closeButton": true}
                toastr.error('Something wrong');
            }
        });
    };

    function setNavigationPosition(){

        $.ajax({
            'url': '{{ url('dreamcms/pages/navigation-page-sort') }}',
            'type': 'POST',
            'success': function(response) {
                if(response.status=="success"){

                    toastr.options = {"closeButton": true}
                    toastr.success('Saved');
                } else {
                    toastr.options = {"closeButton": true}
                    toastr.error('Something wrong');
                }
            },
            'error': function(){
                toastr.options = {"closeButton": true}
                toastr.error('Something wrong');
            }
        });
    }

    $(document).ready(function(){

        $(".select2").select2();

        var $sortableTable = $('.sortable');
        if ($sortableTable.length > 0) {
            $sortableTable.sortable({
                handle: '.sortable-handle',
                axis: 'y',
                update: function(a, b){

                    var entityName = $(this).data('entityname');
                    var $sorted = b.item;

                    var $previous = $sorted.prev();
                    var $next = $sorted.next();

                    if ($previous.length > 0) {
                        changePosition({
                            parentId: $sorted.data('parentid'),
                            type: 'moveAfter',
                            entityName: entityName,
                            id: $sorted.data('itemid'),
                            positionEntityId: $previous.data('itemid')
                        });
                    } else if ($next.length > 0) {
                        changePosition({
                            parentId: $sorted.data('parentid'),
                            type: 'moveBefore',
                            entityName: entityName,
                            id: $sorted.data('itemid'),
                            positionEntityId: $next.data('itemid')
                        });
                    } else {
                        console.error('Something wrong!');
                    }
                },
                cursor: "move"
            });
        }
    });
    </script>
@endsection