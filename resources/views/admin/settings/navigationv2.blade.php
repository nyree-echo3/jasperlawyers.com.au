@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
    <link rel="stylesheet"
          href="{{ asset('/components/menu-builder/bootstrap-iconpicker/css/bootstrap-iconpicker.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Navigation</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                <li class="active">Navigation</li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-body">

                    <div class="col-sm-12 col-md-8 col-lg-6">
                        <h3>Navigation</h3>
                        <ul id="myEditor" class="sortableLists list-group"></ul>
                        <button id="save-navigation" type="button" class="btn btn-info pull-right" name="action" value="save">Save Navigation</button>


                        @can('navigation-add-items')
                        <div class="row">
                            <div class="col-sm-12">
                                <form id="frm-edit" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="text">Text</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control item-menu" name="text" id="text" placeholder="Text">
                                        </div>
                                        <input type="hidden" name="icon" class="item-menu">
                                    </div>
                                    <div class="form-group">
                                        <label for="href">URL</label>
                                        <input type="text" class="form-control item-menu" id="href" name="href" placeholder="URL">
                                    </div>

                                    <button type="button" id="btn-update" class="btn btn-primary" disabled><i class="fas fa-sync-alt"></i> Update</button>
                                    <button type="button" id="btn-add" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">

                                <h2>Modules</h2>
                                <table class="table table-hover">
                                    @foreach($modules as $module)
                                    @php
                                        $style = '';
                                        if($module->show[0]){
                                            $style = ' style="display: none"';
                                        }
                                    @endphp

                                    <tr class="navigation-element" id="module-{{ $module->id }}"{!! $style !!}>
                                        <td style="width:80%">{{ $module->display_name }}</td>
                                        <td>
                                            <button type="button" data-module="{{ $module->slug }}" data-type="module" data-slug="{{ $module->slug }}" data-id="module-{{ $module->id }}" data-text="{{ $module->display_name }}" data-url="{{ $module->slug }}" class="btn btn-sm btn-success pull-right de-add"><i class="fas fa-plus"></i> Add</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>

                                <h2>Categories</h2>
                                <table class="table table-hover">
                                @foreach($categories as $key => $value)

                                    @foreach($value as $category)

                                    @php
                                        $style = '';
                                        if($category->show[0]){
                                            $style = ' style="display: none"';
                                        }

                                        $page_ids = '';
                                        if($category->module_slug=='pages'){

                                            foreach($category->pages as $page){
                                               $page_ids = $page_ids.'-'.$page->id;
                                            }
                                        }

                                    @endphp
                                    <tr class="navigation-element" id="{{ $key }}-{{ $category->id }}"{!! $style !!}>
                                        <td style="width:80%">{{ $key }} / {{ $category->name }}</td>
                                        <td>
                                            <button type="button" data-pageids="{{ $page_ids }}" data-module="{{ $category->module_slug }}" data-type="category" data-slug="{{ $category->slug }}" data-id="{{ $key }}-{{ $category->id }}" data-text="{{ $category->name }}" data-url="{{ $category->url }}" class="btn btn-sm btn-success pull-right de-add"><i class="fas fa-plus"></i> Add</button>
                                        </td>
                                    </tr>
                                    @endforeach

                                @endforeach
                                </table>

                                <h2>Pages</h2>
                                <table class="table table-hover">
                                    @foreach($pages as $page)
                                        @php
                                            $style = '';
                                            if($page->show[0]){
                                                $style = ' style="display: none"';
                                            }
                                        @endphp

                                        <tr class="navigation-element" id="page-{{ $page->id }}"{!! $style !!}>
                                            <td style="width:80%">{{ $page->title }}</td>
                                            <td>
                                                <button type="button" data-module="pages" data-type="page" data-slug="{{ $page->slug }}" data-id="page-{{ $page->id }}" data-text="{{ $page->title }}" data-url="{{ $page->url }}" class="btn btn-sm btn-success pull-right de-add"><i class="fas fa-plus"></i> Add</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>

                            </div>
                        </div>
                        @endcan

                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/menu-builder/bootstrap-iconpicker/js/bootstrap-iconpicker.js') }}"></script>
    <script src="{{ asset('/components/menu-builder/jquery-menu-editor.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">


        $(document).ready(function () {

            // icon picker options
            var iconPickerOptions = {searchText: "Buscar...", labelHeader: "{0}/{1}"};
            // sortable list options
            var sortableListOptions = {
                placeholderCss: {'background-color': "#cccccc"}
            };
            var editor = new MenuEditor('myEditor', {listOptions: sortableListOptions, iconPicker: iconPickerOptions});
            editor.setForm($('#frm-edit'));
            editor.setUpdateButton($('#btn-update'));
            //Calling the update method
            $("#btn-update").click(function () {
                editor.update();
            });
            // Calling the add method
            $('#btn-add').click(function () {
                editor.add();
            });

            $.ajax({
                type: "POST",
                url: "/dreamcms/settings/get-navigation",
                success: function (response) {
                    editor.setData(response.navigation);
                }
            });

            $('.de-add').click(function () {
                editor.addManual($(this).data('module'), $(this).data('id'), $(this).data('type'), $(this).data('slug'),  $(this).data('text'), $(this).data('url'), $(this).data('pageids'));
                $('#'+$(this).data('id')).hide();
            });

            $('#save-navigation').click(function () {
                var nav = editor.getString();

                $.ajax({
                    type: "POST",
                    url: "/dreamcms/settings/save-navigation",
                    data:  {
                        'navigation':nav
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('Navigation has been updated');
                        }
                    }
                });

            });
        });
    </script>
@endsection