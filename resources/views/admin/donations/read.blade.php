@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/donations') }}"><i class="fa fa-envelope"></i> {{ $display_name }}</a></li>
                <li class="active">View</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">                         
                          <div class="box-tools pull-right">
                            @if($previous)
                            <a href="{{ url('dreamcms/donations/'.$previous.'/read') }}" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Previous"><i class="fa fa-chevron-left"></i> Previous</a>
                            @endif
                            @if($next)
                            <a href="{{ url('dreamcms/donations/'.$next.'/read') }}" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Next">Next <i class="fa fa-chevron-right"></i></a>
                            @endif
                          </div>
                        </div>
                        <div class="box-body no-padding">
                          
                          <div class="table-responsive">
                              <table class="table">                                                                   
                                  @foreach(json_decode($donation->data) as $field)
                                  <tr>
                                      <th style="width:20%">{{ strip_tags($field->label) }} :</th>
                                      <td>{{ $field->value }}</td>
                                  </tr>
                                  @endforeach
                                  
                                  <!--<tr>
                                      <th style="width:20%">Amount :</th>
                                      <td>${{ $donation->amount }}</td>
                                  </tr>-->
                              </table>
                          </div>
                          
                          <div class="table-responsive">
                              <h5><strong>&nbsp;&nbsp;PAYMENT</strong></h5>
                              <table class="table">                                                                   
                                  <tr>
                                      <th style="width:20%">Type :</th>
                                      <td>{{ $donation->payment_type }}</td>
                                  </tr>
                                  
                                  <tr>
                                      <th style="width:20%">Status :</th>
                                      <td>{{ ucwords($donation->payment_status) }}</td>
                                  </tr>
                                  
                                  <tr>
                                      <th style="width:20%">Transaction Number :</th>
                                      <td>{{ $donation->payment_transaction_number }}</td>
                                  </tr>
                                  
                                  <tr>
                                      <th style="width:20%">Transaction Result :</th>
                                      <td><span class="{{ ($donation->payment_transaction_result == 'Successful' ? 'text-success' : 'text-danger') }}">{!! ($donation->payment_transaction_result == 'Successful' ? '<i class="fas fa-check"></i> ' : '<i class="fas fa-times"></i> ') !!}{{ $donation->payment_transaction_result }}</span></td>
                                  </tr>                                                                    
                              </table>
                          </div>
                          
                          <div class="box-footer">
                             <a href="{{ url('dreamcms/donations') }}"type="button" class="btn btn-default"><i class="fas fa-undo"></i> Return</a>
						  </div>
                        </div>                       
                      </div> 

                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
  <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection